SSID = "YOUR NETWORK NAME"
PASSWORD = "YOUR NETWORK PASSWORD"
NTP_DELTA = 2208988800-10800
HOST = "pool.ntp.org"
BRIGHTNESS = 1 # 0 to 15
LANGUAGE = "Tr" # Tr or En
# For weather forecast, for example "https://rss.accuweather.com/rss/liveweather_rss.asp?metric=1&locCode=EUR|TR|06420|ANKARA|" We pull RSS information
# Returns value in metric=1 Celsius - metrik=2 Fahrenhayt
# locCode is|CONTINENT|COUNTRY|POST CODE|PROVINCE| It should be given as and Turkish letters should not be used.
# for example, for istanbul-silivri "https://rss.accuweather.com/rss/liveweather_rss.asp?metric=1&locCode=EUR|TR|34570|ISTANBUL|"
# To convert RSS output to JSON data, we use the service on the site "https://api.rss2json.com/v1/api.json?rss_url="
WEATHER_BACKUP = "https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Frss.accuweather.com%2Frss%2Fliveweather_rss.asp%3Fmetric%3D1%26locCode%3DEUR%7CTR%7C34570%7CISTANBUL%7C"
# Required information for accuweather api
# apikey: xxXXXXxXXxxxXxXXXXxXxXxxXXXXxXXx
# lokationkey: 318232 # Silivri
WEATHER_API_TR = "https://dataservice.accuweather.com/forecasts/v1/hourly/1hour/318232?apikey=xxXXXXxXXxxxXxXXXXxXxXxxXXXXxXXx&language=tr-tr&details=true&metric=true"
WEATHER_API = "https://dataservice.accuweather.com/forecasts/v1/hourly/1hour/318232?apikey=xxXXXXxXXxxxXxXXXXxXxXxxXXXXxXXx&language=en-en&details=true&metric=true"
WEATHER_API_LOCATION = "Silivri"
# To convert RSS output to JSON data, we use the service on the site "https://api.rss2json.com/v1/api.json?rss_url="
# NEWS = "https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.cnnturk.com%2Ffeed%2Frss%2Fguncel"
# NEWS = "https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.sozcu.com.tr%2Ffeeds-son-dakika"
# With the membership feature of the service, we can increase the number of news captured to 25.
NEWS_TR = "https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Ft24.com.tr%2Frss&api_key=yyyyyYYyyyyyyyYyyyyyyyyyyyyyyyyyyyYyyYyy&count=25"
NEWS = "https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Frss.nytimes.com%2Fservices%2Fxml%2Frss%2Fnyt%2FWorld.xml&api_key=yyyyyYYyyyyyyyYyyyyyyyyyyyyyyyyyyyYyyYyy&count=25"

