# Open Source Clock with Raspberry Pi Pico W
#
# This project, shared as open source, belongs to a clock project made using MicroPython with Raspberry Pi Pico W.
# Circuit diagrams and all parts used are included in the project files. The codes and the whole project are licensed using the 
# GNU General Public License v3.0. The features provided by the software and hardware for the clock are as follows.
#
# - After the circuit is run, it connects to the internet via wireless network using the information entered in the secrets.py file.
# - It retrieves the most accurate time information from the NTP service immediately after the internet connection is made and every day at 05:00 and 17:00.
# - Even if the power is cut off using the battery in the RTC module used in the project, time information will not be lost.
# - Using the temperature and humidity sensor, this information is displayed on the watch screen at regular intervals.
# - It receives updated weather information from RSS sources every hour as an API service and backup and displays this information on the screen at regular intervals.
# - It receives current news headlines from RSS sources every hour and displays this information on the screen at regular intervals.
# - It shows time - date - ambient temperature and humidity level information on the screen at regular intervals even if there is no internet connection.
# - Do not forget to change the secrets.py file specifically for you. In this file, the necessary settings are made for the project to run properly.
# - In order to benefit from the services without any problems, you must create free memberships on the "https://developer.accuweather.com/" and "https://api.rss2json.com" sites.
# - Currently, Turkish and English interfaces are supported.
#
#  created 2024
#  by Türker Girgin <turkergirgin@gmail.com>
#
#  This project is licensed under the GNU General Public License v3.0.


#Loading required libraries
from machine import Pin, SPI
import network
import secrets
from max7219 import Matrix8x8
from max7219 import GLYPHS
from utime import sleep
import socket
import struct
import time
import ds1302
from dht11 import DHT11, InvalidChecksum
import urequests
import json

#The global variables to be used are defined.
old_sec=999
siralama = 0
hava_siralama = 0
haber_siralama = 0
weather_forecast = {}
news_list = {}

# WLAN is preparing...
wlan = network.WLAN(network.STA_IF)
led = Pin("LED", Pin.OUT)

# Led Matrix Display is preparing...
spi = SPI(0, baudrate=10000000, polarity=1, phase=0, sck=Pin(2), mosi=Pin(3))
ss = Pin(5, Pin.OUT)
display = Matrix8x8(spi, ss, 8)
display.brightness(secrets.BRIGHTNESS)
display.zero()

# Initialize DS1302 RTC with specified pins (clk, dio, cs)
ds = ds1302.DS1302(Pin(0),Pin(1),Pin(4))

# Initialize DHT11
dht11_pin = Pin(28, Pin.OUT, Pin.PULL_DOWN)
dht11_sensor = DHT11(dht11_pin)

def kayan_yazi(yazi = '', bekleme = 0.01): 
    length = len(yazi)
    length = (length*8)
    for x in range(32, -length, -1):
        display.text_from_glyph(yazi, GLYPHS,x,0)
        display.show()
        if bekleme != 0:
            sleep(bekleme)
        display.fill(0)

def connect_wlan():
    wlan.active(False)
    led.off()
    wlan.active(True) # power up the WiFi chip
    if secrets.LANGUAGE == "Tr":
        kayan_yazi('Wifi çipinin aktive olması bekleniyor...')
    else:
        kayan_yazi('Waiting for wifi chip to power up...')
    wlan.connect(secrets.SSID, secrets.PASSWORD) #Connecting to WiFi 
    if secrets.LANGUAGE == "Tr":
        kayan_yazi('Bağlanılan WiFi Ağ İsmi:' + secrets.SSID, 0.03)
    else:
        kayan_yazi('Connecting to WiFi Network Name:' + secrets.SSID, 0.03)
    if wlan.isconnected():
        led.on()
        if secrets.LANGUAGE == "Tr":
            msg = 'Başarılı! Cihaz IP\'si:' + wlan.ifconfig()[0]
        else:
            msg = 'Success! Device IP:' + wlan.ifconfig()[0]
    else:
        led.off()
        if secrets.LANGUAGE == "Tr":
            msg ='Bağlantı Hatası! secrets.py dosyasını kontrol edin.'
        else:
            msg ='Connect Error! Check your secrets.py file'
    kayan_yazi(msg)
    
def get_time_wlan():
    if secrets.LANGUAGE == "Tr":
        kayan_yazi("NTP senkronizasyonu yapılıyor")
    else:
        kayan_yazi("NTP synchronization is in progress")
    display.zero()
    display.text("  ....  ")
    display.show()        
    NTP_QUERY = bytearray(48)
    NTP_QUERY[0] = 0x1B
    addr = socket.getaddrinfo(secrets.HOST, 123)[0][-1]
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.settimeout(3)
        res = s.sendto(NTP_QUERY, addr)
        msg = s.recv(48)
        s.close()
        val = struct.unpack("!I", msg[40:44])[0]
        t = val - secrets.NTP_DELTA    
        tm = time.gmtime(t)
        machine.RTC().datetime((tm[0], tm[1], tm[2], tm[6] + 1, tm[3], tm[4], tm[5], 0))
        # Set the date and time on the RTC
        ds.year(time.localtime()[0])  # Set the year
        ds.month(time.localtime()[1])    # Set the month
        ds.day(time.localtime()[2])     # Set the day
        ds.hour(time.localtime()[3])    # Set the hour
        ds.minute(time.localtime()[4])  # Set the minute
        ds.second(time.localtime()[5])  # Set the second        
    except:
        if secrets.LANGUAGE == "Tr":
            kayan_yazi("Hata: NTP senkronizasyonu yapılamadı!")
        else:
            kayan_yazi("Error: NTP synchronization failed!")
    
def print_time():
    if hr < 10:
        saat = "0" + str(hr)
    else:
        saat = str(hr)
    if min < 10:
        dakika = "0" + str(min)
    else:
        dakika = str(min)
    if sec < 10:
        saniye = "0" + str(sec)
    else:
        saniye = str(sec)
    display.zero()
    display.text(saat+":"+dakika+":"+saniye)
    display.show()
    
def print_date():
    if D < 10:
        gun = "0" + str(D)
    else:
        gun = str(D)
    if M < 10:
        ay = "0" + str(M)
    else:
        ay = str(M)
    yil = str(Y)
    if day == 1:
        if secrets.LANGUAGE == "Tr":
            gun_adi = "Pazartesi"
        else:
            gun_adi = "Monday"
    elif day == 2:
        if secrets.LANGUAGE == "Tr":
            gun_adi = "Salı"
        else:
            gun_adi = "Tuesday"
    elif day == 3:
        if secrets.LANGUAGE == "Tr":
            gun_adi = "Çarşamba"
        else:
            gun_adi = "Wednesday"
    elif day == 4:
        if secrets.LANGUAGE == "Tr":
            gun_adi = "Perşembe"
        else:
            gun_adi = "Thursday"
    elif day == 5:
        if secrets.LANGUAGE == "Tr":
            gun_adi = "Cuma"
        else:
            gun_adi = "Friday"
    elif day == 6:
        if secrets.LANGUAGE == "Tr":
            gun_adi = "Cumartesi"
        else:
            gun_adi = "Saturday"
    elif day == 7:
        if secrets.LANGUAGE == "Tr":
            gun_adi = "Pazar"
        else:
            gun_adi = "Sunday"
    else:
        if secrets.LANGUAGE == "Tr":
            gun_adi = "Enteresan?"
        else:
            gun_adi = "Interesting?"
    kayan_yazi(yil+"/"+ay+"/"+gun+" "+gun_adi, 0.03)
    
def print_measured(test='temp'):
    display.zero()
    if secrets.LANGUAGE == "Tr":
        display.text_from_glyph("Ölçüm...", GLYPHS)
    else:
        display.text_from_glyph("Measure.", GLYPHS)
    display.show()
    if test == 'temp':
        try:
            temp = dht11_sensor.temperature
            time.sleep(1)
        except:
            temp = 999
        if temp != 999:
            if secrets.LANGUAGE == "Tr":
                kayan_yazi("Sıcaklık: {}°C".format(temp), 0.03)
            else:
                kayan_yazi("Temperature: {}°C".format(temp), 0.03)
    else:
        try:
            humidity = dht11_sensor.humidity
            time.sleep(1)
        except:
            humidity = 999
        if humidity != 999:
            if secrets.LANGUAGE == "Tr":
                kayan_yazi("Nem: {}% RH".format(humidity), 0.03)
            else:
                kayan_yazi("Humidity: {}% RH".format(humidity), 0.03)
                
def get_weather_backup():
    global hava_siralama
    hava_siralama = 0
    if secrets.LANGUAGE == "Tr":
        kayan_yazi("Tekrar deneniyor")
    else:
        kayan_yazi("Trying again")
    display.zero()
    display.text("  ....  ")
    display.show()      
    try:
        weather = urequests.get(secrets.WEATHER_BACKUP)
        data = json.loads(weather.content)
        weather_forecast.clear()
        a=0
        for item in data['items']:
            if a == 0 :
                weather_forecast[a] = item['title']
            if a == 0 :
                weather_forecast[a+1] = item['description']
                weather_forecast[a+1] = weather_forecast[a+1].strip('<img')
                weather_forecast[a+1] = weather_forecast[a+1][0:weather_forecast[a+1].find('<')-5]
            if a == 1 :
                weather_forecast[a+1] = item['description']
                weather_forecast[a+1] = weather_forecast[a+1].strip('<img')
                weather_forecast[a+1] = weather_forecast[a+1][0:weather_forecast[a+1].find('<')-5]
            a+=1
    except:
        weather_forecast.clear()
    
    if len(weather_forecast) < 3:
        if secrets.LANGUAGE == "Tr":
            kayan_yazi("Hata: Hava durumu bilgisi alınamadı!")
        else:
            kayan_yazi("Error: Weather information could not be retrieved!")

def print_weather():
    global hava_siralama
    if len(weather_forecast) > 0:
        if secrets.LANGUAGE == "Tr":
            kayan_yazi("Hava Durumu: {}".format(weather_forecast[hava_siralama]), 0.01)
        else:
            kayan_yazi("Weather Forecast: {}".format(weather_forecast[hava_siralama]), 0.01)
        hava_siralama+=1
        if hava_siralama == len(weather_forecast):
            hava_siralama = 0
            
def get_weather_api():
    global hava_siralama
    hava_siralama = 0
    if secrets.LANGUAGE == "Tr":
        kayan_yazi("Hava durumu bilgisi alınıyor")
    else:
        kayan_yazi("Getting weather information")
    display.zero()
    display.text("  ....  ")
    display.show()      
    try:
        if secrets.LANGUAGE == "Tr":
            weather = urequests.get(secrets.WEATHER_API_TR)
        else:
            weather = urequests.get(secrets.WEATHER_API)
        data = json.loads(weather.content)
        weather_forecast.clear()
        if secrets.LANGUAGE == "Tr":
            weather_forecast[0] = secrets.WEATHER_API_LOCATION + " için sıcaklık " + str(data[0]['Temperature']['Value']) + "°C - " +  data[0]['IconPhrase']
            weather_forecast[1] = secrets.WEATHER_API_LOCATION + " için hissedilen sıcaklık " + str(data[0]['RealFeelTemperature']['Value']) + "°C - " +  data[0]['RealFeelTemperature']['Phrase']
            weather_forecast[2] = secrets.WEATHER_API_LOCATION + " için rüzgar " + str(data[0]['Wind']['Speed']['Value']) + " km/h hızında ve " +  str(data[0]['Wind']['Direction']['Degrees']) + " derece yönünde"
            weather_forecast[3] = secrets.WEATHER_API_LOCATION + " için dış ortam nem " + str(data[0]['RelativeHumidity']) + "% RH ve iç ortam nem " +  str(data[0]['IndoorRelativeHumidity']) + "% RH"
            weather_forecast[4] = secrets.WEATHER_API_LOCATION + " için olasılıklar; Yağmur: %" + str(data[0]['RainProbability']) + ", Fırtına: %" +  str(data[0]['ThunderstormProbability']) + ", Kar: %" + str(data[0]['SnowProbability'])
        else:
            weather_forecast[0] = "Temperature for " + secrets.WEATHER_API_LOCATION + " " + str(data[0]['Temperature']['Value']) + "°C - " +  data[0]['IconPhrase']
            weather_forecast[1] = "The felt temperature for " + secrets.WEATHER_API_LOCATION + " " + str(data[0]['RealFeelTemperature']['Value']) + "°C - " +  data[0]['RealFeelTemperature']['Phrase']
            weather_forecast[2] = "Wind for " + secrets.WEATHER_API_LOCATION + " " + str(data[0]['Wind']['Speed']['Value']) + " km/h at speed and " +  str(data[0]['Wind']['Direction']['Degrees']) + " degree direction"
            weather_forecast[3] = "Outdoor humidity for " + secrets.WEATHER_API_LOCATION + " " + str(data[0]['RelativeHumidity']) + "% RH and indoor humidity " +  str(data[0]['IndoorRelativeHumidity']) + "% RH"
            weather_forecast[4] = "Possibilities for " + secrets.WEATHER_API_LOCATION + "; Rain: %" + str(data[0]['RainProbability']) + ", Storm: %" +  str(data[0]['ThunderstormProbability']) + ", Snow: %" + str(data[0]['SnowProbability'])            
    except:
        weather_forecast.clear()
    
    if len(weather_forecast) < 5:
        if data['Code'] ==  "ServiceUnavailable":
            if secrets.LANGUAGE == "Tr":
                kayan_yazi("Hata: Servis kullanılamıyor!")
            else:
                kayan_yazi("Error: Service Unavailable!")
            get_weather_backup()
        else:
            if secrets.LANGUAGE == "Tr":
                kayan_yazi("Hata: Hava durumu bilgisi alınamadı!")
            else:
                kayan_yazi("Error: Weather information could not be retrieved!")
            
def get_news():
    global haber_siralama
    haber_siralama = 0
    if secrets.LANGUAGE == "Tr":
        kayan_yazi("Haber listesi alınıyor")
    else:
        kayan_yazi("Retrieving news list")
    display.zero()
    display.text("  ....  ")
    display.show()      
    try:
        if secrets.LANGUAGE == "Tr":
            news = urequests.get(secrets.NEWS_TR)
        else:
            news = urequests.get(secrets.NEWS)
        data = json.loads(news.content)
        news_list.clear()
        a=0
        for item in data['items']:
            news_list[a] = item['title']
            a+=1
        #for i in range(0, len(news_list)):
        #    print(news_list[i])    
    except:
        news_list.clear()
    
    if len(news_list) == 0:
        if secrets.LANGUAGE == "Tr":
            kayan_yazi("Hata: Haber listesi alınamadı!")
        else:
            kayan_yazi("Error: Failed to retrieve news list!")
            
def print_news():
    global haber_siralama
    if len(news_list) > 0:
        if secrets.LANGUAGE == "Tr":
            kayan_yazi("Haber: {}".format(news_list[haber_siralama]),0)
        else:
            kayan_yazi("News: {}".format(news_list[haber_siralama]),0)
        haber_siralama+=1
        if haber_siralama == len(news_list)-1:
            haber_siralama = 0            

#By connecting to the wireless network, the necessary information is retrieved from the services.
connect_wlan()
if wlan.isconnected():
    get_time_wlan()
if wlan.isconnected():    
    get_weather_api()
if wlan.isconnected():    
    get_news()    
       
while True:
    (Y,M,D,day,hr,min,sec)=ds.date_time()

    #if (day == 7) and (hr == 4) and (min == 30) and (sec == 3):
    #    machine.reset()
        
    if ((hr == 5) and (min == 0) and (sec == 5)) or ((hr == 17) and (min == 0) and (sec == 5)):
        if wlan.isconnected():
            get_time_wlan()
        else:
            connect_wlan()
            if wlan.isconnected():
                get_time_wlan()

    if (min == 15) and (sec == 5):
        if wlan.isconnected():
            get_weather_api()
        else:
            connect_wlan()
            if wlan.isconnected():
                get_weather_api()
            
    if (min == 45) and (sec == 5):
        if wlan.isconnected():
            get_news()
        else:
            connect_wlan()
            if wlan.isconnected():
                get_news()                
                
    if (sec == 6) or (sec == 36):
        if siralama == 0:
            print_date()
        elif siralama == 1:
            print_measured('temp')
        elif siralama == 2:
            print_measured('humidity')
        elif siralama == 3:
            print_weather()
        elif siralama == 4:
            print_news()
        siralama+=1
        if siralama == 5:
            siralama = 0
        
    if old_sec != sec:
        print_time()
    
    old_sec = sec

    
