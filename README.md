# Picow Clock



###### This project, shared as open source, belongs to a clock project made using MicroPython with Raspberry Pi Pico W. Circuit diagrams and all parts used are included in the project files. The codes and the whole project are licensed using the GNU General Public License v3.0.  
###### The features provided by the software and hardware for the clock are as follows.

 - After the circuit is run, it connects to the internet via wireless network using the information entered in the secrets.py file.
 - It retrieves the most accurate time information from the NTP service immediately after the internet connection is made and every day at 05:00 and 17:00.
 - Even if the power is cut off using the battery in the RTC module used in the project, time information will not be lost.
 - Using the temperature and humidity sensor, this information is displayed on the watch screen at regular intervals.
 - It receives updated weather information from RSS sources every hour as an API service and backup and displays this information on the screen at regular intervals.
 - It receives current news headlines from RSS sources every hour and displays this information on the screen at regular intervals.
 - It shows time - date - ambient temperature and humidity level information on the screen at regular intervals even if there is no internet connection.
 - Do not forget to change the secrets.py file specifically for you. In this file, the necessary settings are made for the project to run properly.
 - In order to benefit from the services without any problems, you must create free memberships on the "https://developer.accuweather.com/" and "https://api.rss2json.com" sites.
 - Currently, Turkish and English interfaces are supported.

 ***Breadboard Connections***

![Open Source DIY Clock with Arduino](https://gitlab.com/turkergirgin/picow-clock/-/raw/889d2fed771a40ff96dc23a9d44748b4f11b06b8/Project%20Files/picow_clock.png)  

***Component List***

| Amount | Part Type |
| ------ | ------ |
| 1 | Raspberry Pi Pico W |
| 1 | DHT11 Humitidy and Temperature Sensor (3 pins) |
| 2 | 8x8 DOT MATRIX BOARD MAX 7219 | 
| 1 | RTC_DS1302_module | 
